from django.shortcuts import render
from django.views.generic import TemplateView,ListView
from .models import producto, categoria
from .forms import CategoriaForm, SubcategoriaForm,ProductoForm
from django.core.paginator import Paginator,PageNotAnInteger




# Create your views here.

class productoView(TemplateView):
    template_name = "productos/index.html"
      
    def get_queryset(self):           
        queryset = producto.objects.all()
        return queryset                
    
    def get_context_data(self, **kwargs):
        
        #traer  6 valores de cada categoria
        kwargs['veterinaria']=producto.objects.filter(categoria_id=1).order_by('precio')[:6] #//recordar que categoria_id viene heredado
        kwargs['medicina']=producto.objects.filter(categoria_id=2).order_by('precio')[:6]
        kwargs['deporte']=producto.objects.filter(categoria_id=3)[:6]
        kwargs['comida']=producto.objects.filter(categoria_id=4)[:6]

        
        return kwargs





class ProductoDetallado(TemplateView):
    template_name = 'productos/productox.html'
    
    def get_context_data(self, **kwargs):
        libro_slug = kwargs.get('slug') #del url recibe que eslug quiere
        kwargs['productoyo'] = producto.objects.get(slug=libro_slug)
        return kwargs


#seccion de revision de anuncios

class ProductoRevision(TemplateView):
    template_name = 'productos/productorevision.html'
    def get_context_data(self, **kwargs):
        #agrupar automaticamente       
        qonda=producto.objects.order_by('modified')
        #seleccion de grupos de 4 en 4 --- solo cambiar al numero a 20 si se requiere
        paginas=Paginator(qonda,2)             
        i=0 
        barrabuscar = self.request.GET.get('buscar')

        if barrabuscar:   
            try:
                i=(int(barrabuscar)-1)*5  
            except ValueError:
                i=0
            
       
        for a in range(5):
            i=i+1
            if  0<i <= paginas.num_pages:          
                kwargs['bloque'+str(a)]=paginas.page(i) 
           
        return kwargs



#vista del formularioque quiza use luego
class FormularioCategoriaView(TemplateView):
    template_name = "productos/formulario_autor.html"
    #CategoriaForm es la clase q hereda de forms 
    def get_context_data(self, **kwargs):
        kwargs['form'] = CategoriaForm   
        return kwargs

    def post(self, request, *args, **kwargs):
        contexto = self.get_context_data(**kwargs)
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save()   
        contexto['form'] = form
        return self.render_to_response(contexto)


#vista formulario subcategoria


class FormularioSubcategoriaView(TemplateView):
    template_name = "productos/formulario_subcategoria.html"

    def get_context_data(self, **kwargs):
        kwargs['formita'] = SubcategoriaForm
        return kwargs

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = SubcategoriaForm(request.POST)
        if form.is_valid():
            form.save()
        context['formita'] = form
        return self.render_to_response(context)


class FormularioProductoView(TemplateView):
    template_name = "productos/formulario_productos.html"

    def get_context_data(self, **kwargs):
        kwargs['esteesunvalormanejable'] = ProductoForm
        return kwargs

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = ProductoForm(request.POST)
        if form.is_valid():
            form.save()
        context['esteesunvalormanejable'] = form
        return self.render_to_response(context)
